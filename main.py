import logging
import sys

import numpy as np

from analysis import analyze_data
from utils import read_file, log_result, generate_ascii_graph

logging.basicConfig(
    encoding="utf-8",
    level=logging.INFO,
    format="%(asctime)s.%(msecs)03d %(levelname)s: %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
    handlers=[logging.FileHandler("task.log"), logging.StreamHandler(sys.stdout)],
)


def main():
    data = read_file('scores.txt')
    scores_np = np.array(data)

    result = analyze_data(scores_np)
    log_result(result)

    one_std_deviation_away = result['mean'] + result['standard_deviation']
    values_one_std = scores_np[scores_np >= one_std_deviation_away]  # 4. საშუალოზე ერთი სტანდარტული დევიაციით და მეტით
    logging.info(
        "Values that are more than one standard deviation away from the mean: %s",
        ", ".join(map(str, values_one_std)),
    )

    filtered_score = scores_np[scores_np > one_std_deviation_away]
    graph = generate_ascii_graph(scores_np, filtered_score)
    logging.info("ASCII Graph:\n%s", graph)


if __name__ == '__main__':
    main()
