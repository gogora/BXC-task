import logging

import numpy as np


def read_file(file_name: str) -> list:
    with open(file_name, 'r') as file:
        file_data = file.readline()
        return [int(i) for i in file_data.split(',')]


def log_result(data: dict) -> None:
    for key, value in data.items():
        logging.info(f"{key}: {value}")


def generate_ascii_graph(scores_np: np.ndarray, filtered_score: list) -> str:
    score, counts = np.unique(scores_np, return_counts=True)
    score_mapper = dict(zip(score, counts))
    max_frequency = max(score_mapper.values())

    graph = ""
    for freq in range(max_frequency, 0, -1):
        graph += f"{freq} | "
        for score in range(min(scores_np), max(scores_np) + 1):
            if score_mapper.get(score, 0) >= freq:
                graph += "  ^" if score in filtered_score else "  *"
                continue
            graph += "   "
        graph += "\n"

    graph += "    +" + "-" * (max(scores_np) - min(scores_np) + 1) * 3 + "\n"
    graph += "     "
    for score in range(min(scores_np), max(scores_np) + 1):
        graph += f"{score:3d}"

    return graph
