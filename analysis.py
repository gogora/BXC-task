import numpy as np


def analyze_data(scores: np.ndarray) -> dict:
    standard_deviation = np.std(scores, ddof=1)
    return {
        'mean': scores.mean(),
        'standard_deviation': standard_deviation,
        'standard_error': standard_deviation / np.sqrt(np.size(scores))
    }
