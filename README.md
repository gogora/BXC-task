# BXC Task

```bash
pip3 install -r requirements.txt
```

## Usage

```
python3 main.py
```

## License

[MIT](https://choosealicense.com/licenses/mit/)